
// grecaptcha.ready(function() {
//   grecaptcha.execute('6LfD37cZAAAAAJG3g0pL0t1DdmSAlny3DP2_EbJG', {action: 'homepage'}).then(function(token) {
//     console.log( token );
//     $('#btnRegistro').show();
//   });
// });


$(document).ready(function() {
    $(function() {
        var $hamburger = $('.hamburger'),
            $nav = $('#site-nav'),
            $masthead = $('#masthead');

        $hamburger.click(function() {
          $(this).toggleClass('is-active');
          $nav.toggleClass('is-active');
          $masthead.toggleClass('is-active');
          return false; 
        })
    });
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
           $('#masthead').addClass('header-fixed');
        } else {
           $('#masthead').removeClass('header-fixed');
        }
    });


    $('#abrir1').click(function() {
        $('#tlegales').fadeIn(300);
    });
    $('#close i').click(function() {
        $('#tlegales').fadeOut(300);
        console.log('click')
    });

    //Formulario 
    $('form#registro .form-row-inner .celular').keypress(function(event) {
        // console.log(event.which);
        if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
            event.preventDefault();
        }
    });

    $("form#registro .form-row-inner .nombre").keypress(function(event) {
        var inputValue = event.charCode;
        if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    $("form#registro .form-row-inner .apellido").keypress(function(event) {
        var inputValue = event.charCode;
        if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });

    $('form#registro .form-row-inner .email').on('keypress', function() {
        var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
    });

    $('.fade').slick({
      dots: false,
      arrows: false,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 4000,
      speed: 1500,
      fade: true,
      cssEase: 'linear'
    });

    // Variables contador
    var tiempo = {}; 
    var clock = new Date("2020-08-020 07:00:00 PM"); // Obtener la fecha y almacenar en clock
    var intervalo = window.setInterval(mostrar_hora, 1); // Frecuencia de actualización
    var i = 0; // Esta variable define los estados de intervalo

    function mostrar_hora(){
      var now = new Date();  
      // Inserta la hora almacenada en clock en el span con id hora
      tiempo.horas = document.getElementById('hora');
      tiempo.horas.innerHTML = "0" + clock.getHours() - now.getHours(); 
      
      // Inserta los minutos almacenados en clock en el span con id minuto
      tiempo.minuto = document.getElementById('minuto');
      tiempo.minuto.innerHTML = clock.getMinutes()+60 - now.getMinutes();
      
      // Inserta los segundos almacenados en clock en el span con id segundo
      tiempo.segundos = document.getElementById('segundo')
      tiempo.segundos.innerHTML = "0" + clock.getSeconds()+60 - now.getSeconds();
      
    }

    //Slider Banners
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        autoplay: true
    });
    $('.slider-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        arrows: true,
        prevArrow:'<div class="prev"><img src="assets/img/arrow_prev.png"></div>',
        nextArrow: '<div class="next"><img src="assets/img/arrow_next.png"></div>',
        centerMode: true,
        focusOnSelect: true,
        infinite: true,
        centerPadding: '0px'
    });

    // Slider Access
    var alterClass = function() {
        var ww = document.body.clientWidth;
        if (ww < 767) {
            $('.access').addClass('center');
        } else if (ww >= 768) {
            $('.access').removeClass('center');
        };
    };
    $(window).resize(function(){
        alterClass();
    });
    //Fire it when the page first loads:
    alterClass();

    $('.center').slick({
        dots: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        infinite: false,
        autoplay: false,
        centerMode: true,
        centerPadding: '40px',
        autoplaySpeed: 4000
    });


});

$(function() {
	
    $('#btnRegistro').click(function(verifL) {
        var nombre = $("#nombres").val();
        var apellido = $("#apellidos").val();
        var tipoDoc = $("#tipoDoc").val();
        var dni = $("#dni").val();
        var email = $("#email").val();
        var tyc = $("#tyc").prop("checked");

		if (nombre.length == 0) {
			mostrarError("lbNombres", "Ingresa tus nombres.");
			return false;
		} else if (apellido.length == 0) {
			mostrarError("lbApellidos", "Ingresa tus apellidos.");
			return false;
		} else if (tipoDoc == "") {
			mostrarError("lbTipoDoc", "Elige el tipo de documento.");
			return false;
		} else if ((tipoDoc == "DNI" && dni.length != 8) || (tipoDoc != "DNI" && dni.length < 8)) {
			mostrarError("lbDNI", "Ingresa un número de documento válido.");
			return false;
		} else if (!verifEmail(email)) {
			mostrarError("lbEmail", "Ingresa un email válido.");
			return false;
		} else if (!tyc) {
			mostrarError("lbTYC", "Acepta los términos y condiciones.");
			return false;
		} else {
			try {
				$("#btnRegistro").hide();
				validarDNI(dni, nombre, apellido, email, tipoDoc, tyc);
			} catch (ex) { return false; }
		}
    });	
	

    function ocultarErrores() {
        $("#lbNombres").hide();
        $("#lbApellidos").hide();
        $("#lbTipoDoc").hide();
        $("#lbDNI").hide();
        $("#lbEmail").hide();
        $("#lbTYC").hide();
    }

    function verifL(n) {
        permitidos = /[^áàäâªÁÀÂÄéèëêÉÈÊËíìïîÍÌÏÎóòöôÓÒÖÔ'úùüûÚÙÛÜabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ .]/;
        if (permitidos.test(n.value.slice(n.value.length - 1))) {
            //n.value = n.value.slice(0, n.value.length - 1)
            n.value = "";
            //n.focus();
        }
    }

    function verifTelefono(n) {
        permitidos = /[^áàäâªÁÀÂÄéèëêÉÈÊËíìïîÍÌÏÎóòöôÓÒÖÔ'úùüûÚÙÛÜabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ .]/;
        if (!permitidos.test(n.value.slice(n.value.length - 1))) {
            //n.value = n.value.slice(0, n.value.length - 1)
            n.value = "";
            //n.focus();
        }
        if (n.value.length > 9) {
            n.value = n.value.slice(0, 9);
        }
    }

    function verifDNI(n) {
        permitidos = /[^áàäâªÁÀÂÄéèëêÉÈÊËíìïîÍÌÏÎóòöôÓÒÖÔ'úùüûÚÙÛÜabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ .]/;
        if (!permitidos.test(n.value.slice(n.value.length - 1))) {
            //n.value = n.value.slice(0, n.value.length - 1)
            n.value = "";
            // n.focus();
        }
        if (n.value.length > 11) {
            n.value = n.value.slice(0, 11);
        }
    }

    function verifEmail(n) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(n)) {
            return true;
        } else {
            return false;
        }
    }

    function validarDNI(dni, nombre, apellido, email, tipoDoc) {

        $.ajax({
                url: 'assets/php/lib/servicios.php',
                type: 'POST',
                data: {
                    'dni': dni,
                    'nombres': nombre,
                    'apellidos': apellido,
                    'tipoDoc': tipoDoc,
                    'email': email,
                },
            })
            .done(function(respuesta) {
                //console.log(respuesta);
                var resultado = JSON.parse(respuesta);
                if (resultado.status == 1) {
                    dniValid = true;
                    //validarEmail(nombre, email, celular);             
                    window.open('gracias.html', '_self')
                } else {
                    dniValid = false;
                    mostrarError("lbDNI", resultado.mensaje);
                    $("a[href$='next']").show();
                    return false;
                }
            })
            .fail(function() {
                //console.log("error");
                dniValid = false;
                $("a[href$='next']").show();
                return false
            });
    }

});